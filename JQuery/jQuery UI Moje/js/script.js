 /* global $ */


$(function(){

$('.box').draggable();



$('#box1').draggable({ scroll: true, revert: 'invalid'});
	
$('#box2').draggable({ axis: "x"});

$('#box3').draggable({ axis: "y"});

$('#box4').draggable({ containment: '.container', revert: 'invalid'});
					  
					 

$('#droppable').droppable({
	accept: '#box1, #box4',
	drop: function(){
		$(this).text('span').html("when a box got attitude, drop it like it's hot");
	}
	
	
	});









$('#sortable').sortable({ connectWith: '#sortableToo', placeholder: 'placeholderBox'});

$('#sortableToo').sortable({ connectWith: '#sortable', placeholder: 'placeholderBox'});





$('.date').datepicker({
	showOtherMonths: true,
	selectOtherMonths: true,
	showButtonPanel: true,
//	changeMonth: true,
//	changeYear: true,
	numberOfMonths: 2,
	minDate: -5,
//	maxDate: 7
});





$('#todoList').sortable({
	items: "li:not('.listTitle, .addItem')",
	connectWith: "ul",
	dropOnEmpty: true,
	placeholder: '.emptySpace',
});



$('input').keydown(function(e){
	if(e.keyCode == 13){
	   
		var item = $(this).val();
		
		$(this).parent().parent().append('<li>' + item + '</li>');
		$(this).val('');
		
	   }
});

	$('#trash').droppable({
		drop: function(event, ui){
			ui.draggable.remove();
		}
	});
	
	

});



















































